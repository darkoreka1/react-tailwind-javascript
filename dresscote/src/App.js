import { BrowserRouter, Routes, Route } from "react-router-dom";
import StyleDashboard from "./components/Dashboard/StyleDashboard";
import StyleDetails from "./components/Dashboard/StyleDetails";
import AddGenre from "./components/Forms/AddGenre";
import Login from "./components/Forms/Login";
import HomePage from "./components/HomePage/HomePage";
import Register from "./components/Forms/Register";
import Navbar from "./components/Navbar/Navbar";
import AddStyle from "./components/Forms/AddStyle";
import AppLayout from "./utils/AppLayout";
import Profile from "./components/Profile/Profile";
import EditProfile from "./components/Profile/EditProfile";
import AllProfiles from "./components/Profile/AllProfiles";
// import StylePosts from "./components/Posts/StylePosts";
import { LikeProvider } from "./components/context/PostsContext/LikeContext";
import Posts from "./components/Posts/Posts";

function App() {
  return (
    <LikeProvider>
      <AppLayout>
        <BrowserRouter>
          <Navbar />
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/add-genre/:id" element={<AddGenre />} />
            <Route path="/dashboard" element={<StyleDashboard />} />
            <Route path="/style-details/:styleID" element={<StyleDetails />} />
            <Route path="/dashboard/styles/create" element={<AddStyle />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/edit-profile" element={<EditProfile />} />
            <Route path="/all-profiles" element={<AllProfiles />} />
            <Route path="/edit-profile/:profileId" element={<EditProfile />} />
            <Route path="/posts" element={<Posts />} />
          </Routes>
        </BrowserRouter>
      </AppLayout>
    </LikeProvider>
  );
}

export default App;
