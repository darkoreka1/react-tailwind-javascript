import React from "react";
import backgroundImg from "../components/staff/background.jpg";

const AppLayout = ({ children }) => {
  return (
    <div
      className="app-layout"
      style={{
        backgroundImage: `url(${backgroundImg})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      {children}
    </div>
  );
};

export default AppLayout;
