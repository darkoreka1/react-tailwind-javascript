import { useContext, useEffect, useState } from 'react';
import { genreContext } from '../context/GenreContext/GenreContext';
import { useNavigate, useParams } from 'react-router-dom';

export default function AddGenre() {
  const { id } = useParams(); //getting id from url
  const { createGenreAction } = useContext(genreContext);
  const navigate = useNavigate();
  const [genreAdded, setGenreAdded] = useState(false);
  const [formData, setFormData] = useState({
    name: '',
    genreType: 'Pop',
    notes: '',
  });
  //handle form change
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  //handle form submit
  const handleSubmit = (e) => {
    e.preventDefault();
    createGenreAction({ style: id, ...formData });
    setGenreAdded(true);
  };

  useEffect(() => {
    if (genreAdded) {
      navigate(`/style-details/${id}`);
    }
  }, [genreAdded, navigate, id]);

  const backgroundStyle = {
    backgroundImage: 'url("/path-to-your-background-image.jpg")',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    minHeight: '100vh', // background to cover
  };

  return (
    <>
      <div style={backgroundStyle}>
        <div className="flex min-h-full flex-col justify-center py-12 sm:px-6 lg:px-8">
          <div className="sm:mx-auto sm:w-full sm:max-w-md">
            <h2 className="mt-6 text-center text-3xl font-bold tracking-tight text-gray-900">
              Add Genre
            </h2>
          </div>

          <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
            <div className="py-8 px-4 shadow sm:rounded-lg sm:px-10">
              <form className="space-y-6" onSubmit={handleSubmit}>
                <div>
                  <label className="block text-sm font-medium text-gray-700">
                    Name
                  </label>
                  <div className="mt-1">
                    <input
                      name="name"
                      value={formData.name}
                      onChange={handleChange}
                      type="text"
                      className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                    />
                  </div>
                </div>
                <div>
                  <label className="block text-sm font-medium text-gray-700">
                    Genre Type
                  </label>
                  <select
                    name="genreType"
                    value={formData.genreType}
                    onChange={handleChange}
                    className="mt-1 block w-full border-2 rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  >
                    <option value="Pop">Pop </option>
                    <option value="Rock">Rock </option>
                    <option value="Hip-hop/Rap">Hip-hop/Rap </option>
                    <option value="Electronic Dance Music (EDM)">
                      Electronic Dance Music (EDM){' '}
                    </option>
                    <option value="Country">Country </option>
                    <option value="Jazz">Jazz </option>
                    <option value="Classical">Classical </option>
                    <option value="R&B/Soul">R&B/Soul </option>
                    <option value="Uncategorized">Uncategorized </option>
                  </select>
                </div>

                <div>
                  <label className="block text-sm font-medium text-gray-700">
                    Add Note
                  </label>
                  <div className="mt-1">
                    <textarea
                      rows={4}
                      name="notes"
                      value={formData.notes}
                      onChange={handleChange}
                      className="block w-full border-2 rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                    />
                  </div>
                </div>
                <div>
                  <button
                    type="submit"
                    className="flex w-full  justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                  >
                    Add New Genre
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
