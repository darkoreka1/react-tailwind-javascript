import { useContext, useEffect, useState } from 'react';
import { styleContext } from '../context/StyleContext/StyleContext';
import { useNavigate } from 'react-router-dom';

export default function AddStyle() {
  const { createStyleAction } = useContext(styleContext); //error kiszedve
  const navigate = useNavigate(); //remelem nem kotnek bele
  const [formData, setFormData] = useState({
    name: '',
    styleType: 'T-shirts',
    category: 'Business attire',
    notes: '',
    imageUrls: '',
  });

  const [formSubmitted, setFormSubmitted] = useState(false);

  //handle form change
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  //handle form submit
  const handleSubmit = (e) => {
    e.preventDefault();
    createStyleAction(formData);
    setFormSubmitted(true);
  };
  //useEffect for ensure that the route is refreshed
  useEffect(() => {
    if (formSubmitted) {
      navigate('/dashboard');
    }
  }, [formSubmitted, navigate]);

  return (
    <>
      <div className="flex min-h-full flex-col justify-center py-12 sm:px-6 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-md">
          <h2 className="mt-6 text-center text-6xl font-bold tracking-tight text-gray-900">
            Add Style
          </h2>
        </div>

        <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
          <div className=" py-8 px-4 shadow sm:rounded-lg sm:px-10">
            <form className="space-y-6" onSubmit={handleSubmit}>
              <div>
                <label className="block text-sm font-medium text-white">
                  Name
                </label>
                <div className="mt-1">
                  <input
                    value={formData.name}
                    onChange={handleChange}
                    name="name"
                    type="text"
                    className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  />
                </div>
              </div>

              <div>
                <label className="block text-sm font-medium text-white">
                  Style Type
                </label>
                <select
                  name="styleType"
                  value={formData.styleType}
                  onChange={handleChange}
                  className="mt-1 block w-full border-2 rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                >
                  <option value="T-shirts">T-shirts</option>
                  <option value="Jeans">Jeans</option>
                  <option value="Sweaters">Sweaters</option>
                  <option value="Dresses">Dresses</option>
                  <option value="Skirts">Skirts</option>
                  <option value="Shorts">Shorts</option>
                  <option value="Suits">Suits</option>
                  <option value="Uncategorized">Uncategorized</option>
                </select>
              </div>

              <div>
                <label className="block text-sm font-medium text-white">
                  Category
                </label>
                <select
                  name="category"
                  value={formData.category}
                  onChange={handleChange}
                  className="mt-1 block w-full border-2 rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                >
                  <option value="Business attire">Business attire</option>
                  <option value="Casual wear">Casual wear</option>
                  <option value="Formal wear">Sweaters</option>
                  <option value="Sportswear">Dresses</option>
                  <option value="AT home">Skirts</option>
                  <option value="Trips">Shorts</option>
                  <option value="Uncategorized">Suits</option>
                </select>
              </div>
              <div>
                <label className="block text-sm font-medium text-white">
                  Add Note
                </label>
                <div className="mt-1">
                  <textarea
                    rows={4}
                    name="notes"
                    type="text"
                    value={formData.notes}
                    onChange={handleChange}
                    className="block w-full border-2 rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>
              </div>

              <div>
                <label className="block text-sm font-medium text-white">
                  Add Image URL
                </label>
                <div className="mt-1">
                  <textarea
                    rows={4}
                    name="imageUrls"
                    type="text"
                    value={formData.imageUrls}
                    onChange={handleChange}
                    className="block w-full border-2 rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  />
                </div>
              </div>

              <div>
                <button
                  type="submit"
                  className="flex w-full  justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >
                  Add New Style
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
