import { React, useContext, useEffect } from 'react';
import PostComponent from './PostComponent';
import { styleContext } from '../context/StyleContext/StyleContext';

const PostList = () => {
  const { getAllStylesDetailsAction, styles } = useContext(styleContext); //get data from database -fetchprofileaction
  //dispatch action
  useEffect(() => {
    //when component mounted useeffect triggers fetchprflaction
    getAllStylesDetailsAction();
  }, [getAllStylesDetailsAction]);

  return (
    <section className="py-24">
      <div className="container px-4 mx-auto">
        <div className="max-w-3xl mx-auto">
          {styles.length === 0 ? (
            <>
              <p className="mb-4 text-xl font-medium text-white">
                No Styles Found
              </p>
              <p className="mb-4 text-lg text-white">
                You have not created any styles yet
              </p>
              <a
                href="/dashboard/styles/create"
                className="px-6 py-3 text-lg font-medium text-white bg-indigo-600 rounded-md hover:bg-indigo-700"
              >
                Create Style
              </a>
            </>
          ) : (
            <>
              <div className="max-w-4xl mx-auto mb-12 text-center">
                <h3 className="mb-4 text-3xl md:text-4xl leading-tight text-white font-bold tracking-tighter">
                  List Of Styles
                </h3>
                <p className="text-lg md:text-xl text-white font-medium">
                  A list of users styles
                </p>
              </div>
              {styles?.map((style) => {
                return (
                  <PostComponent
                    key={style._id}
                    style={style}
                    genres={style.genres}
                  />
                );
              })}
            </>
          )}
        </div>
      </div>
    </section>
  );
};

export default PostList;
