import React from "react";
import { useLike } from "../context/PostsContext/LikeContext";

const HeartIcon = ({ styleId }) => {
  const { likeStyle, unlikeStyle, isStyleLiked } = useLike();

  const handleLikeClick = () => {
    if (isStyleLiked(styleId)) {
      unlikeStyle(styleId); // Unlike the style
    } else {
      likeStyle(styleId); // Like the style
    }
  };

  return (
    <span
      className={`cursor-pointer ${
        isStyleLiked(styleId) ? " bg-red-900 rounded" : "text-gray-500"
      }`}
      onClick={handleLikeClick}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        strokeWidth="1.5"
        stroke="currentColor"
        className="w-6 h-6"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
        />
      </svg>
    </span>
  );
};

export default HeartIcon;
