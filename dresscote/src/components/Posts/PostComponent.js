import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { API_URL_USER } from '../../utils/apiURL';
import axios from 'axios';
import HeartIcon from './LikeIcon';

const PostComponent = ({ style, genres }) => {
  const [username, setUsername] = useState('');
  const [profilePicture, setProfile] = useState('');

  useEffect(() => {
    if (style?.createdBy == null) {
      return;
    }
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const loadData = async () => {
      try {
        const resp = await axios.get(
          `${API_URL_USER}/${style?.createdBy}/fullname`,
          config
        );
        console.log('Createdby', style?.createdBy);
        setUsername(resp.data.name);
        console.log('Resp post', resp.data.name);
        setProfile(resp.data.profilePicture);
      } catch (error) {
        console.log(error);
      }
    };

    loadData();
  }, [style?.createdBy]); //style?.createdBy

  //console.log('Genre: ', genre);

  return (
    <div className="post-container bg-white border border-gray-300 rounded-lg mb-4">
      <div className="post-header flex items-center p-4">
        <img
          src={profilePicture}
          alt="User Profile"
          className="w-10 h-10 rounded-full mr-2"
        />
        <div>
          <Link
            to={`/style-details/${style?._id}`}
            className="font-semibold text-black"
          >
            {style?.name}
          </Link>
          <p className="text-gray-500 text-sm">{username}</p>
        </div>
      </div>
      <div className="post-image">
        <img src={style?.imageUrls} alt="Style" className="w-full" />
      </div>
      <div className="post-actions p-4">
        <div className="flex items-center space-x-4">
          <HeartIcon styleId={style?._id} />
        </div>
        {genres?.map((genre) => {
          return (
            <p key={genre._id} className="text-gray-500 text-sm">
              Genre: {genre?.name}
            </p>
          );
        })}
        {/* Display genre */}
        <p className="text-gray-500 text-sm">Note: {style?.notes}</p>{' '}
        {/* Display note */}
      </div>
    </div>
  );
};

export default PostComponent;
