import React, { useState, useEffect, useContext } from 'react';
import { authContext } from '../context/AuthContext/AuthContext';

//ez meg nem megy sajnos
const EditProfile = () => {
  const [updatedUserData, setUpdatedUserData] = useState({});
  const [isEditing, setIsEditing] = useState(false);

  const { updateProfileAction, fetchProfileAction, profile, error } =
    useContext(authContext);

  useEffect(() => {
    fetchProfileAction();
  }, []);

  console.log(profile?.data?.fullname);

  const handleUpdateProfile = async () => {
    try {
      // Assuming you have the form data prepared
      const updatedProfile = await updateProfileAction(updatedUserData);

      // You can optionally fetch the updated profile data after the update
      // or update the local state with the updated data.
      // Example (fetch updated profile data):
      // await fetchProfileAction();

      console.log('Profile updated successfully:', updatedProfile);
    } catch (error) {
      console.error('Error updating profile:', error);
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUpdatedUserData({ ...updatedUserData, [name]: value });
  };

  const handleEditClick = () => {
    setIsEditing(true);
  };

  const handleSaveClick = () => {
    // Send updated user data to the server
    handleUpdateProfile();
  };

  const backgroundStyle = {
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    minHeight: '100vh', // background to cover
  };

  return (
    <>
      {error ? (
        <>
          <div
            className="bg-red-100 border text-center border-red-400 text-red-700 px-4 py-3 rounded relative"
            role="alert"
          >
            <strong className="font-bold">Error!</strong>{' '}
            <span className="block sm:inline ">{error}</span>
          </div>
        </>
      ) : (
        <div style={backgroundStyle}>
          <div className=" min-h-screen">
            <div className="max-w-3xl mx-auto p-4 sm:p-6 lg:p-8">
              <div className="bg-white opacity-80 rounded-lg shadow-md p-6">
                <h1 className="text-3xl font-bold text-gray-800 mb-6">
                  Edit Profile
                </h1>

                <div className="mb-4">
                  <h2 className="text-xl font-semibold text-gray-700">
                    User Information
                  </h2>
                </div>

                <div className="grid grid-cols-2 gap-4">
                  <div className="col-span-2 sm:col-span-1">
                    <label
                      htmlFor="fullname"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Full Name
                    </label>
                    {isEditing ? (
                      <input
                        type="text"
                        id="fullname"
                        name="fullname"
                        value={updatedUserData.fullname || ''}
                        onChange={handleInputChange}
                        className="mt-1 p-2 w-full rounded-md border border-gray-300 focus:outline-none focus:border-indigo-500"
                      />
                    ) : (
                      <p className="mt-1 text-lg text-gray-900">
                        {profile?.data?.fullname}
                      </p>
                    )}
                  </div>

                  <div className="col-span-2 sm:col-span-1">
                    <label
                      htmlFor="email"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Email
                    </label>
                    {isEditing ? (
                      <input
                        type="email"
                        id="email"
                        name="email"
                        value={updatedUserData.email || ''}
                        onChange={handleInputChange}
                        className="mt-1 p-2 w-full rounded-md border border-gray-300 focus:outline-none focus:border-indigo-500"
                      />
                    ) : (
                      <p className="mt-1 text-lg text-gray-900">
                        {profile?.data?.email}
                      </p>
                    )}
                  </div>
                </div>
                <div className="px-4 py-4 sm:px-6">
                  {isEditing ? (
                    <button
                      onClick={handleSaveClick}
                      className="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-lg font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                    >
                      Save
                    </button>
                  ) : (
                    <button
                      onClick={handleEditClick}
                      className="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-lg font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                    >
                      Edit Profile
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default EditProfile;
