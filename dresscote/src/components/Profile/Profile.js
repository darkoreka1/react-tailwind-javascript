import { authContext } from '../context/AuthContext/AuthContext';
import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AllProfiles from './AllProfiles';

const Profile = () => {
  const { fetchProfileAction, profile, error } = useContext(authContext);

  useEffect(() => {
    fetchProfileAction();
  }, []);

  console.log(profile?.data);

  const backgroundStyle = {
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    minHeight: '100vh', // background to cover
  };

  return (
    <>
      {error ? (
        <>
          <div
            className="bg-red-100 border text-center border-red-400 text-red-700 px-4 py-3 rounded relative"
            role="alert"
          >
            <strong className="font-bold">Error!</strong>{' '}
            <span className="block sm:inline ">{error}</span>
          </div>
        </>
      ) : (
        <div style={backgroundStyle}>
          <div className=" min-h-screen">
            <div className="max-w-3xl mx-auto p-4 sm:p-6 lg:p-8">
              <div className="bg-white opacity-80 rounded-lg shadow-md p-6">
                <div className="mb-4">
                  <h1 className="text-3xl font-bold text-gray-800">Profile</h1>
                  <p className="mt-2 text-xl font-bold text-gray-700">
                    User Profile Information
                  </p>
                </div>
              </div>

              <div className="mt-8">
                <div className="bg-white opacity-80 shadow overflow-hidden sm:rounded-lg">
                  <div className="px-4 py-5 sm:px-6">
                    <h3 className="text-2xl leading-6 font-medium text-gray-900">
                      User Information
                    </h3>
                  </div>
                  <div className="border-t border-gray-200 px-4 py-5 sm:p-0">
                    <dl className="sm:divide-y sm:divide-gray-200">
                      <div className="sm:flex sm:items-center">
                        <dt className="text-lg font-medium text-gray-500 sm:w-1/4">
                          Full Name
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                          {profile?.data?.fullname}
                        </dd>
                      </div>

                      <div className="sm:flex sm:items-center">
                        <dt className="text-lg font-medium text-gray-500 sm:w-1/4">
                          Email
                        </dt>
                        <dd className="mt-1 text-lg text-gray-900 sm:mt-0 sm:col-span-2">
                          {profile?.data?.email}
                        </dd>
                      </div>
                    </dl>
                  </div>
                </div>
              </div>
              <div className="mt-4 sm:mt-8">
                <Link
                  to="/edit-profile"
                  className="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-lg font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >
                  Edit Profile
                </Link>
              </div>
            </div>
            {/* ide kell tenni egy feltetelt h ha admin akkor lassa ezt s ha nem akkor ne */}
            <AllProfiles />
          </div>
          {/* profiles={profile?.data} */}
        </div>
      )}
    </>
  );
};

export default Profile;
