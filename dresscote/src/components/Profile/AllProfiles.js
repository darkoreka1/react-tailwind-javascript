import React, { useContext, useEffect, useState } from 'react';
import { authContext } from '../context/AuthContext/AuthContext';
import { useNavigate } from 'react-router-dom';

const AllProfiles = () => {
  const { getAllProfileDetailsAction, deleteProfileAction, profiles, error } =
    useContext(authContext);
  const navigate = useNavigate();
  const [profileDeleted, setProfileDeleted] = useState(false);

  useEffect(() => {
    getAllProfileDetailsAction();
  }, []);

  console.log(profiles?.data);

  const backgroundStyle = {
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    minHeight: '100vh', // background to cover
  };

  const handleDeleteProfile = (profileId) => {
    // Call the deleteProfileAction with the profileId to delete the profile
    deleteProfileAction(profileId);
    setProfileDeleted(true);
  };

  useEffect(() => {
    if (profileDeleted) {
      navigate('/profile');
    }
  }, [profileDeleted, navigate]);

  return (
    <>
      {error ? (
        <>
          <div
            className="bg-red-100 border text-center border-red-400 text-red-700 px-4 py-3 rounded relative"
            role="alert"
          >
            <strong className="font-bold">Error!</strong>{' '}
            <span className="block sm:inline ">{error}</span>
          </div>
        </>
      ) : (
        <div style={backgroundStyle}>
          <section className="py-24">
            <div className="container px-4 mx-auto">
              <h1 className="mb-8 text-3xl font-semibold text-white">
                User List
              </h1>
              {profiles?.data?.length <= 0 ? (
                <>
                  <p className="mb-4 text-xl font-medium text-white">
                    No Profiles Found
                  </p>
                  <p className="mb-4 text-lg text-white">
                    There are no user profiles available.
                  </p>
                  {/* Link to create profile */}
                </>
              ) : (
                <ul className="space-y-4">
                  {profiles?.data?.map((profile) => (
                    <li
                      key={profile._id} // Use _id as the unique key
                      className="bg-white rounded-lg shadow-md p-4 flex items-center justify-between"
                    >
                      <div>
                        {profile.fullname}
                        <p className="text-gray-500">{profile.email}</p>
                      </div>
                      <div className="space-x-2">
                        {/* Edit Profile Button */}
                        {/* <Link
                          key={profile._id}
                          to={`/edit-profile/${profiles._id}`}
                          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                        >
                          View profile
                        </Link> */}
                        {/* Delete Profile Button */}

                        <button
                          className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                          onClick={() => handleDeleteProfile(profile._id)}
                        >
                          Delete Profile
                        </button>
                      </div>
                    </li>
                  ))}
                </ul>
              )}
            </div>
          </section>
        </div>
      )}
    </>
  );
};

export default AllProfiles;
