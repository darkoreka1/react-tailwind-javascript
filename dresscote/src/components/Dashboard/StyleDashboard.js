import { useContext, useEffect, useState } from 'react';
import { authContext } from '../context/AuthContext/AuthContext';
import StyleList from './StyleList';
import StyleSummery from './StyleSummery';
import { styleContext } from '../context/StyleContext/StyleContext';

const StyleDashboard = () => {
  //authcntxt contains data related to user
  const { fetchProfileAction, profile, error } = useContext(authContext); //get data from database -fetchprofileaction
  //dispatch action
  useEffect(() => {
    //when component mounted useeffect triggers fetchprflaction
    fetchProfileAction();
  }, [fetchProfileAction]); //because empty array  runs only once after the initial render

  // const [styles, setStyles] = useState(profile?.data?.styles || []); // Initialize with your styles

  // const handleDeleteStyle = (styleId) => {
  //   // Update the styles state by filtering out the deleted style
  //   setStyles((prevStyles) =>
  //     prevStyles.filter((style) => style._id !== styleId)
  //   );
  // };

  return (
    <>
      {error ? (
        <>
          <div
            className="bg-red-100 border text-center border-red-400 text-red-700 px-4 py-3 rounded relative"
            role="alert"
          >
            <strong className="font-bold">Error!</strong>{' '}
            <span className="block sm:inline ">{error}</span>
          </div>
        </>
      ) : (
        <>
          <StyleSummery />
          <StyleList styles={profile?.data?.styles} />
        </>
      )}
    </>
  );
};

export default StyleDashboard;
