import React from "react";
import modelImg from "../staff/slide7.jpeg";
import modelImg2 from "../staff/slide8.jpeg";
import modelImg3 from "../staff/slide9.jpeg";

const StyleSummery = () => {
  return (
    <>
      <div className="max-w-4xl mx-auto mb-12 text-center">
        <h3
          className="mb-4 text-5xl md:text-6xl leading-tight text-coolGray-900 font-bold tracking-tighter"
          style={{ textShadow: "2px 2px 4px rgba(255, 255, 255, 0.8)" }}
        >
          Total Styles
        </h3>
        <p className="text-lg md:text-xl text-white font-medium">
          A list of your styles
        </p>
      </div>
      <section className="bg-coolGray-50 py-4">
        <div className="container px-4 mx-auto">
          <div className="flex flex-wrap -m-3">
            <div className="w-full md:w-1/3 p-3">
              <img
                src={modelImg}
                alt="Today's Style"
                className="rounded-md shadow-dashboard w-80 h-38 mx-auto opacity-80"
              />
            </div>
            <div className="w-full md:w-1/3 p-3">
              <img
                src={modelImg2}
                alt="Today's Genres"
                className="rounded-md shadow-dashboard w-60 h-38 mx-auto opacity-95"
              />
            </div>
            <div className="w-full md:w-1/3 p-3">
              <img
                src={modelImg3}
                alt="Balance"
                className="rounded-md shadow-dashboard w-80 h-38 mx-auto opacity-80"
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default StyleSummery;
