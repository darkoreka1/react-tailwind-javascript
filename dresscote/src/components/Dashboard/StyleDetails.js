import { Link, useParams } from "react-router-dom";
import { PlusIcon } from "@heroicons/react/20/solid";
import AllGenres from "./AllGenres";
import { useContext, useEffect } from "react";
import { styleContext } from "../context/StyleContext/StyleContext";

//mindenkepp atnezni
export default function StyleDetails() {
  const { getStyleDetailsAction, style } = useContext(styleContext);
  const { styleID } = useParams(); //getting styleid from url
  useEffect(() => {
    getStyleDetailsAction(styleID);
  }, [styleID]);
  console.log("Style:", style);

  const backgroundStyle = {
    backgroundImage: 'url("/path-to-your-background-image.jpg")',
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center center",
    minHeight: "100vh", // background to cover
  };

  return (
    <div style={backgroundStyle}>
      <>
        {style?.genres?.length <= 0 ? (
          <>
            <h2 className="text-center text-red-500 m-10 bg-white p-4 rounded">
              This Style Don't have any genre
            </h2>

            <div className="text-center">
              <Link
                to={`/add-genre/${styleID}`}
                className="relative inline-flex items-center rounded-md border border-transparent bg-indigo-500 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 focus:ring-offset-gray-800"
              >
                <PlusIcon className="-ml-1 mr-2 h-5 w-5" aria-hidden="true" />
                <span>New Genre</span>
              </Link>
            </div>
          </>
        ) : (
          <>
            <div className=" pt-12 sm:pt-16">
              <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                <div className="mx-auto max-w-4xl text-center">
                  <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                    {style?.name}
                  </h2>
                  <p className="mt-3 text-xl text-gray-500 sm:mt-4">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                    Repellendus repellat laudantium.
                  </p>
                </div>
              </div>
              {/* <div className="mt-10 pb-12 sm:pb-16">
                <div className="relative">
                  <div className="absolute inset-0 h-1/2" />
                  <div className="relative mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                    <div className="mx-auto max-w-4xl">
                      <dl className="rounded-lg bg-white shadow-lg sm:grid sm:grid-cols-3">
                        <div className="flex flex-col border-b border-gray-100 p-6 text-center sm:border-0 sm:border-r">
                          <dt className=" mt-2 text-lg font-medium leading-6 text-gray-500">
                            Total Style
                          </dt>
                          <dd className=" text-5xl font-bold tracking-tight text-indigo-600">
                            4000
                          </dd>
                        </div>
                        <div className="flex flex-col border-t border-b border-gray-100 p-6 text-center sm:border-0 sm:border-l sm:border-r">
                          <dt className=" mt-2 text-lg font-medium leading-6 text-gray-500">
                            Total Styles
                          </dt>
                          <dd className=" text-5xl font-bold tracking-tight text-indigo-600">
                            3000
                          </dd>
                          <Link
                            to={`/styles-list/${3}`}
                            className="  font-bold tracking-tight text-green-600"
                          >
                            View History
                          </Link>
                        </div>
                        <div className="flex flex-col border-t border-gray-100 p-6 text-center sm:border-0 sm:border-l">
                          <dt className=" mt-2 text-lg font-medium leading-6 text-gray-500">
                            Total Style
                          </dt>
                          <dd className=" text-5xl font-bold tracking-tight text-indigo-600">
                            900
                          </dd>
                          <Link
                            to={`/styles-list/`}
                            className="  font-bold tracking-tight text-green-600"
                          >
                            View History
                          </Link>
                        </div>
                      </dl>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>

            <AllGenres genres={style?.genres} styleID={styleID} />
          </>
        )}
      </>
    </div>
  );
}
