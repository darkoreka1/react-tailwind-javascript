import React, { createContext, useContext, useState } from "react";

// Create the Like context
const LikeContext = createContext();

// Provide a custom hook to access the Like context
export function useLike() {
  return useContext(LikeContext);
}

// Create the LikeProvider component
export function LikeProvider({ children }) {
  const [likes, setLikes] = useState([]); // State for storing liked styles

  // Function to like a style
  const likeStyle = (styleId) => {
    // Add the styleId to the likes state
    setLikes([...likes, styleId]);
    // You can also send an API request to actually like the style on the server here
  };

  // Function to unlike a style
  const unlikeStyle = (styleId) => {
    // Remove the styleId from the likes state
    const updatedLikes = likes.filter((id) => id !== styleId);
    setLikes(updatedLikes);
    // You can also send an API request to unlike the style on the server here
  };

  // Determine if a style is liked
  const isStyleLiked = (styleId) => {
    return likes.includes(styleId);
  };

  return (
    <LikeContext.Provider value={{ likeStyle, unlikeStyle, isStyleLiked }}>
      {children}
    </LikeContext.Provider>
  );
}
