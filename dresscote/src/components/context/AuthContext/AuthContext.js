import { createContext, useReducer } from 'react';
import axios from 'axios';
import {
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  FETCH_PROFILE_FAIL,
  FETCH_PROFILE_SUCCESS,
  LOGOUT,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  UPDATE_PROFILE,
  UPDATE_PROFILE_FAIL,
  FETCH_ALLPROFILE_SUCCESS,
  FETCH_ALLPROFILE_FAIL,
  DELETE_PROFILE,
  DELETE_PROFILE_FAIL,
  GET_USERNAME,
  GET_USERNAME_FAIL,
} from './authActionTypes';
import { API_URL_USER } from '../../../utils/apiURL';

//auth context to provide authentication related data to components
export const authContext = createContext();

//Initial state
const INITIAL_STATE = {
  userAuth: JSON.parse(localStorage.getItem('userAuth')), //storeing user auth data, get it from the localstorage
  error: null,
  loading: false,
  profile: null, //no user data initially
  profiles: [],
  name: null,
};

//Auth Reducer

const reducer = (state, action) => {
  const { type, payload } = action; //payload-additional data associated with the action
  switch (type) {
    //Register
    case REGISTER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        userAuth: payload,
      };
    case REGISTER_FAIL:
      return {
        ...state,
        error: payload,
        loading: false,
        userAuth: null,
      };

    //Login
    case LOGIN_SUCCESS:
      //Add user to localstorage
      localStorage.setItem('userAuth', JSON.stringify(payload));
      return {
        ...state,
        loading: false,
        error: null,
        userAuth: payload,
      };
    case LOGIN_FAILED:
      return {
        ...state,
        error: payload,
        loading: false,
        userAuth: null,
      };

    //Profile reducer
    case FETCH_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        profile: payload,
      };
    case FETCH_PROFILE_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
        profile: null,
      };

    //All profiles
    case FETCH_ALLPROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        profiles: payload,
      };
    case FETCH_ALLPROFILE_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
        profiles: null,
      };

    //Get username
    case GET_USERNAME:
      return {
        ...state,
        loading: false,
        error: null,
        name: payload,
      };
    case GET_USERNAME_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
        name: null,
      };

    //Update Profile
    case UPDATE_PROFILE:
      return {
        ...state,
        loading: false,
        error: null,
        profile: payload,
      };
    case UPDATE_PROFILE_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
        profile: null,
      };

    //Delete Profile
    case DELETE_PROFILE:
      return {
        ...state,
        loading: false,
        error: null,
        profile: payload,
      };
    case DELETE_PROFILE_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
        profile: null,
      };

    //Logout
    case LOGOUT:
      //remove user from local storage
      localStorage.removeItem('userAuth');
      return {
        ...state,
        loading: false,
        error: null,
        userAuth: null,
      };

    default:
      return state;
  }
};

//Provider
const AuthContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);

  //login action
  const loginUserAction = async (formData) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const res = await axios.post(`${API_URL_USER}/login`, formData, config);
      if (res?.data?.status === 'success') {
        //if I have response and data on response then use the status
        dispatch({
          type: LOGIN_SUCCESS,
          payload: res.data,
        });
      }
      //Redirect to dashboard after succesful login
      window.location.href = '/dashboard';
    } catch (error) {
      dispatch({
        type: LOGIN_FAILED,
        payload: error?.response?.data?.message,
      });
    }
  };

  //REGISTER action
  const registerUserAction = async (formData) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      //Send a POST request to the server with form data
      const res = await axios.post(
        `${API_URL_USER}/register`,
        formData,
        config
      );
      if (res?.data?.status === 'success') {
        //if I have response and data on response then use the status
        dispatch({
          type: REGISTER_SUCCESS,
          payload: res.data,
        });
      }
      //Redirect
      window.location.href = '/login';
    } catch (error) {
      dispatch({
        type: REGISTER_FAIL,
        payload: error?.response?.data?.message,
      });
    }
  };

  //profile action
  const fetchProfileAction = async () => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${state?.userAuth?.token}`,
        },
      };
      const res = await axios.get(`${API_URL_USER}/profile`, config); //, formData

      if (res?.data) {
        dispatch({
          type: FETCH_PROFILE_SUCCESS, //load data from user profile
          payload: res?.data,
        });
      }
      //window.location.href = "/profile";
    } catch (error) {
      dispatch({
        type: FETCH_PROFILE_FAIL,
        payload: error?.response?.data?.message,
      });
    }
  };

  //get username
  const getUsername = async (userId) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${state?.userAuth?.token}`,
        },
      };
      const res = await axios.get(`${API_URL_USER}/${userId}/fullname`, config); //, formData

      if (res?.data) {
        dispatch({
          type: GET_USERNAME,
          payload: res?.data.name,
        });
      }
    } catch (error) {
      dispatch({
        type: GET_USERNAME_FAIL,
        payload: error?.response?.data?.message,
      });
    }
  };

  //Get all profiles details action
  const getAllProfileDetailsAction = async () => {
    //helyes
    const config = {
      headers: {
        Authorization: `Bearer ${state?.userAuth?.token}`,
        'Content-Type': 'application/json',
      },
    };
    try {
      const res = await axios.get(`${API_URL_USER}`, config);
      //console.log("Api res: ", res?.data); mukodik
      console.log('Config auth', config);
      if (res?.data?.status === 'success') {
        //dispatch
        dispatch({
          type: FETCH_ALLPROFILE_SUCCESS,
          payload: res?.data,
        });
      }
    } catch (error) {
      console.log(error); //itt lett meg eloszor a hiba
      dispatch({
        type: FETCH_ALLPROFILE_FAIL,
        payload: error.data,
      });
    }
  };

  //update profile
  const updateProfileAction = async (formData) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${state?.userAuth?.token}`,
        },
      };
      const res = await axios.put(`${API_URL_USER}`, formData, config);

      if (res?.data?.status === 'success') {
        dispatch({
          type: UPDATE_PROFILE,
          payload: res.data, // Update the user's profile with the new data
        });
      }
    } catch (error) {
      dispatch({
        type: UPDATE_PROFILE_FAIL,
        payload: error?.response?.data?.message,
      });
    }
  };

  // Delete Profile
  const deleteProfileAction = async (profileId) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${state?.userAuth?.token}`,
        },
      };
      const res = await axios.delete(`${API_URL_USER}/${profileId}`, config);

      if (res?.data?.status === 'success') {
        dispatch({
          type: DELETE_PROFILE,
          payload: res.data, // Assuming you have the profile ID to be deleted
        });
      }
    } catch (error) {
      dispatch({
        type: DELETE_PROFILE_FAIL,
        payload: error?.response?.data?.message,
      });
    }
  };

  //Logout action
  const logoutUserAction = () => {
    dispatch({
      type: LOGOUT,
      payload: null,
    });
    //Redirect
    window.location.href = '/login';
  };

  //console.log("State: ", state?.profiles); mukodik

  return (
    <authContext.Provider
      value={{
        loginUserAction,
        userAuth: state,
        token: state?.userAuth?.token,

        fetchProfileAction,
        profile: state?.profile,
        error: state?.error,

        getAllProfileDetailsAction,
        profiles: state?.profiles,

        getUsername,
        name: state?.name,

        updateProfileAction,

        deleteProfileAction,

        logoutUserAction,

        registerUserAction,
      }}
    >
      {children}
    </authContext.Provider>
  );
};

export default AuthContextProvider;
