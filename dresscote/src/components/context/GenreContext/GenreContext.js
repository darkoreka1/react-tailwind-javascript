import React, { createContext, useReducer } from "react";
import axios from "axios";

import { GENRE_CREATION_SUCCES, GENRE_CREATION_FAIL } from "./genreActionTypes";
import { API_URL_GENRE } from "../../../utils/apiURL";

export const genreContext = createContext();

const INITIAL_STATE = {
  genre: null,
  genres: [],
  loading: false,
  error: null,
  token: JSON.parse(localStorage.getItem("userAuth")),
};
//Reducer
const genreReducer = (state, action) => {
  const { type, payload } = action;
  switch (type) {
    case GENRE_CREATION_SUCCES:
      return {
        ...state,
        loading: false,
        genre: payload,
      };
    case GENRE_CREATION_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
};

//Provider
export const GenreContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(genreReducer, INITIAL_STATE);

  //create genre
  const createGenreAction = async (styleData) => {
    try {
      //header
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${state?.token?.token}`,
        },
      };
      //request
      const res = await axios.post(API_URL_GENRE, styleData, config);
      console.log(res.data);
      if (res?.data?.status === "success") {
        dispatch({ type: GENRE_CREATION_SUCCES, payload: res?.data });
      }
    } catch (error) {
      console.log(error); //hibakereses
      dispatch({
        type: GENRE_CREATION_FAIL,
        payload: error?.response?.data?.message,
      });
    }
  };
  return (
    <genreContext.Provider
      value={{
        genre: state.genre,
        genres: state.genres,
        createGenreAction,
        error: state?.error,
      }}
    >
      {children}
    </genreContext.Provider>
  );
};
