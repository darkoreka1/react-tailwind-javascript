import { createContext, useReducer } from 'react';
import axios from 'axios';
import {
  STYLE_DETAILS_SUCCESS,
  STYLE_DETAILS_FAIL,
  STYLE_CREATION_SUCCES,
  STYLE_CREATION_FAIL,
  STYLE_FETCH_FAIL,
  STYLE_FETCH_SUCCESS,
  STYLE_DELETE_SUCCESS,
  STYLE_DELETE_FAIL,
} from './styleActionTypes';
import { API_URL_STYLE } from '../../../utils/apiURL';

export const styleContext = createContext();

//Initial state

const INITIAL_STATE = {
  userAuth: JSON.parse(localStorage.getItem('userAuth')),
  style: null,
  styles: [],
  loading: false,
  error: null,
};

//reducer
const styleReducer = (state, action) => {
  const { type, payload } = action;
  switch (type) {
    //Details
    case STYLE_DETAILS_SUCCESS:
      return {
        ...state,
        style: payload,
        loading: false,
        error: null,
      };
    case STYLE_DETAILS_FAIL:
      return {
        ...state,
        style: null,
        loading: false,
        error: payload,
      };
    //Create
    case STYLE_CREATION_SUCCES:
      return {
        ...state,
        style: payload,
        loading: false,
        error: null,
      };
    case STYLE_CREATION_FAIL:
      return {
        ...state,
        style: null,
        loading: false,
        error: payload,
      };

    //all styles
    case STYLE_FETCH_SUCCESS:
      return {
        ...state,
        styles: payload,
        loading: false,
        error: null,
      };
    case STYLE_FETCH_FAIL:
      return {
        ...state,
        styles: null,
        loading: false,
        error: payload,
      };

    //delete styles
    case STYLE_DELETE_SUCCESS:
      return {
        ...state,
        style: payload,
        loading: false,
        error: null,
      };
    case STYLE_DELETE_FAIL:
      return {
        ...state,
        style: null,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
};

//provider
export const StyleContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(styleReducer, INITIAL_STATE);

  //Get style details action
  const getStyleDetailsAction = async (id) => {
    //id from ex the localstorage
    const config = {
      headers: {
        Authorization: `Bearer ${state?.userAuth?.token}`,
        'Content-Type': 'application/json',
      },
    };
    try {
      const res = await axios.get(`${API_URL_STYLE}/${id}`, config);

      if (res?.data?.status === 'success') {
        //dispatch
        dispatch({
          type: STYLE_DETAILS_SUCCESS,
          payload: res?.data?.data,
        });
      }
    } catch (error) {
      console.log(error); //itt lett meg eloszor a hiba
      dispatch({
        type: STYLE_DETAILS_FAIL,
        payload: error?.data,
      });
    }
  };

  //Create style details action
  const createStyleAction = async (formData) => {
    console.log(state?.userAuth);
    const config = {
      headers: {
        Authorization: `Bearer ${state?.userAuth?.token}`,
        'Content-Type': 'application/json',
      },
    };
    try {
      const res = await axios.post(`${API_URL_STYLE}`, formData, config);

      if (res?.data?.status === 'success') {
        //dispatch
        dispatch({
          type: STYLE_CREATION_SUCCES,
          payload: res?.data?.response?.message,
        });
      }
      console.log(res);
    } catch (error) {
      console.log(error); //most itt a baj
      dispatch({
        type: STYLE_CREATION_FAIL,
        payload: error?.data,
      });
    }
  };

  //Get all styles details action
  const getAllStylesDetailsAction = async () => {
    //itt nem tudom ha kell az id vagy sem
    const config = {
      headers: {
        Authorization: `Bearer ${state?.userAuth?.token}`,
        'Content-Type': 'application/json',
      },
    };
    try {
      const res = await axios.get(`${API_URL_STYLE}`, config);

      if (res?.status === 200) {
        //dispatch
        dispatch({
          type: STYLE_FETCH_SUCCESS,
          payload: res?.data,
        });
      }
    } catch (error) {
      console.log(error); //itt lett meg eloszor a hiba
      dispatch({
        type: STYLE_FETCH_FAIL,
        payload: error?.data,
      });
    }
  };

  // Delete Style
  const deleteStyleAction = async (id) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${state?.userAuth?.token}`,
        },
      };
      const res = await axios.delete(`${API_URL_STYLE}/${id}`, config);

      if (res?.data?.status === 'success') {
        dispatch({
          type: STYLE_DELETE_SUCCESS,
          payload: res.data, // Assuming you have the profile ID to be deleted
        });
      }
    } catch (error) {
      dispatch({
        type: STYLE_DELETE_FAIL,
        payload: error?.response?.data?.message,
      });
    }
  };

  return (
    <styleContext.Provider
      value={{
        getStyleDetailsAction,
        style: state?.style,
        createStyleAction,
        error: state?.error,

        getAllStylesDetailsAction,
        styles: state?.styles,
        deleteStyleAction,
      }}
    >
      {children}
    </styleContext.Provider>
  );
};
