import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import AuthContextProvider from "./components/context/AuthContext/AuthContext";
import { GenreContextProvider } from "./components/context/GenreContext/GenreContext";
import { StyleContextProvider } from "./components/context/StyleContext/StyleContext";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <AuthContextProvider>
    <StyleContextProvider>
      <GenreContextProvider>
        <App />
      </GenreContextProvider>
    </StyleContextProvider>
  </AuthContextProvider>
);
//collect and report to external services
reportWebVitals();
