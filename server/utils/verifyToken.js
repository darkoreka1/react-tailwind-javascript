const jwt = require('jsonwebtoken');

// nodejs module
const verifyToken = (token) =>
  jwt.verify(token, 'anykey', (err, decoded) => {
    if (err) {
      return false;
    }
    return decoded;
  });

module.exports = verifyToken;
