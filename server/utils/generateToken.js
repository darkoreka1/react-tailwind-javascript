const jwt = require('jsonwebtoken');

const generateToken = (id) => 
   jwt.sign({ id }, 'anykey', { expiresIn: '10d' }) // secure key to verify the tokens authenticity
;

module.exports = generateToken;
