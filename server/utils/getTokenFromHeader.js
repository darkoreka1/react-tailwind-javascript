const getTokenFromHeader = (req) => {
  // how to get the token from header

  if (req.headers && req.headers.authorization) {
    const headerObj = req.headers;
    const token = headerObj.authorization.split(' ')[1]; // because the 0. one is the bearer
    if (token !== undefined) {
      return token;
    } 
      return {
        status: 'failed',
        message: 'There is no token attached to the header',
      };
    
  } 
    return {
      status: 'failed',
      message: 'Authorization header is missing in the request',
    };
  
};

module.exports = getTokenFromHeader;
