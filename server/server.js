const express = require('express');
require('./config/dbConnect.js');
const cors = require('cors');
const styleRoute = require('./routes/styles/styleRoute.js');
const genresRoute = require('./routes/genres/genresRoute.js');
const usersRoute = require('./routes/users/usersRoute.js');
const globalErrHandler = require('./middlewares/globalErrHandler.js');

const app = express();

// middlewares
app.use(express.json()); // parse incoming data,allows to work with json easy
// cors middleware( cross origin resource sharing)
app.use(cors());
// users route
app.use('/api/v1/users', usersRoute);
// account routes
app.use('/api/v1/styles', styleRoute);

// genres route
app.use('/api/v1/genres', genresRoute);

// Error handlers
app.use(globalErrHandler);

// listen to server
const PORT = process.env.PORT || 8080;
app.listen(PORT, console.log(`Server is up and runing on port ${PORT}`));
