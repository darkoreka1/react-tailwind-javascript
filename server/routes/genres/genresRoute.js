const express = require('express');
const {
  createGenreCtrl,
  getGenresCtrl,
  getGenreCtrl,
  deleteGenreCtrl,
  updateGenreCtrl,
} = require('../../controllers/genres/genresCtrl.js');
const isLogin = require('../../middlewares/isLogin.js');

const genresRoute = express.Router();

// POST/api/v1/genres
genresRoute.post('/', isLogin, createGenreCtrl);
// GET/api/v1/genres
genresRoute.get('/', getGenresCtrl);
// GET/api/v1/genres/:id
genresRoute.get('/:id', getGenreCtrl);
// DELETE/api/v1/genres/:id
genresRoute.delete('/:id', deleteGenreCtrl);

// PUT/api/v1/genres/:id
genresRoute.put('/:id', updateGenreCtrl);

module.exports = genresRoute;
