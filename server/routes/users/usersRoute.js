const express = require('express');
const {
  registerUserCtrl,
  userLoginCtrl,
  userProfileCtrl,
  deleteUserCtrl,
  updateUserCtrl,
  getProfilesCtrl,
  getUserNameByUserId,
} = require('../../controllers/users/usersCtrl.js');
const isLogin = require('../../middlewares/isLogin.js');
const isAdmin = require('../../middlewares/isAdmin.js');

const usersRoute = express.Router();

// POST/api/v1/users/register

usersRoute.post('/register', registerUserCtrl);
// POST/api/v1/users/login
usersRoute.post('/login', userLoginCtrl);
// GET/api/v1/users/profile
usersRoute.get('/profile/', isLogin, userProfileCtrl);
// GET/api/v1/users/profile
usersRoute.get('/profileAdmin/', isLogin, isAdmin, userProfileCtrl);
// GET/api/v1/users/profiles
usersRoute.get('/', isLogin, isAdmin, getProfilesCtrl);
// GET/api/v1/users/:id/fullname
usersRoute.get('/:id/fullname', getUserNameByUserId);

// DELETE/api/v1/users/
usersRoute.delete('/:id', deleteUserCtrl); // admin beallitas

// PUT/api/v1/users/
usersRoute.put('/', isLogin, isAdmin, updateUserCtrl); // admin

module.exports = usersRoute;
