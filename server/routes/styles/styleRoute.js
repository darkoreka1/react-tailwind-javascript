const express = require('express');
const {
  createStyleCtrl,
  getStyleCtrl,
  deleteStyleCtrl,
  updateStyleCtrl,
  getStylesCtrl,
  likeStyleCtrl,
  unlikeStyleCtrl,
} = require('../../controllers/styles/styleCtrl.js');
const isLogin = require('../../middlewares/isLogin.js');

const styleRoute = express.Router();

// POST/api/v1/styles
styleRoute.post('/', isLogin, createStyleCtrl);

// GET/api/v1/styles/:id
styleRoute.get('/:id', getStyleCtrl);

// DELETE/api/v1/styles/:id
styleRoute.delete('/:id', deleteStyleCtrl);

// PUT/api/v1/styles/:id
styleRoute.put('/:id', updateStyleCtrl);

// GET/api/v1/styles
styleRoute.get('/', getStylesCtrl);

// POST/api/v1/styles/:id/like
styleRoute.post('/:id/like', isLogin, likeStyleCtrl);

// POST/api/v1/styles/:id/unlike
styleRoute.post('/:id/unlike', isLogin, unlikeStyleCtrl);

module.exports = styleRoute;
