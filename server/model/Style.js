const mongoose = require('mongoose');

// Genre schema
const styleSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    styleType: {
      type: String,
      enum: [
        'T-shirts',
        'Jeans',
        'Sweaters',
        'Dresses',
        'Skirts',
        'Shorts',
        'Suits',
        'Uncategorized',
      ],
      required: true,
    },
    category: {
      type: String,
      enum: [
        'Business attire',
        'Casual wear',
        'Formal wear',
        'Sportswear',
        'AT home',
        'Trips',
        'Uncategorized',
      ],
      required: true,
    },
    genres: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Genre',
      },
    ],
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    notes: {
      type: String,
      required: true,
    },
    likes: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],

    imageUrls: [
      {
        type: String,
      },
    ],
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
  }
);

// model
const Style = mongoose.model('Style', styleSchema);

module.exports = Style;
