const mongoose = require('mongoose');

// user schema
const userSchema = new mongoose.Schema(
  {
    fullname: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    profilePicture: {
      // Add profile picture field
      type: String, // Assuming you store the image URL as a string
    },
    hasCreatedAccount: {
      type: Boolean,
      default: false,
    },
    // admin
    isAdmin: {
      type: Boolean,
      default: false,
    },
    styles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Style',
      },
    ],
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
  }
);

// model
const User = mongoose.model('User', userSchema);

module.exports = User;
