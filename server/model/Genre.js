const mongoose = require('mongoose');

// Genre schema
const genreSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    genreType: {
      type: String,
      enum: [
        'Pop',
        'Rock',
        'Hip-hop/Rap',
        'Electronic Dance Music (EDM)',
        'Country',
        'Jazz',
        'Classical',
        'R&B/Soul',
        'Uncategorized',
      ],
      required: true,
    },
    styles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Style',
      },
    ],
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    notes: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
  }
);

// model
const Genre = mongoose.model('Genre', genreSchema);

module.exports = Genre;
