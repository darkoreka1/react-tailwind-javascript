/* eslint-disable no-multi-assign */
/* eslint-disable no-param-reassign */
const globalErrHandler = (err, req, res, next) => {
  // message
  // status
  // statusCode
  // stack

  // eslint-disable-next-line no-multi-assign
  const statuscode = (err.statusCode = err.statusCode || 500);
  const status = (err.status = err.status || 'error');
  const { message } = err;
  const { stack } = err;
  res.status(statuscode).json({
    status,
    message,
    stack,
  });
  next();
};

module.exports = globalErrHandler;
