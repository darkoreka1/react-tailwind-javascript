const { AppErr } = require('../utils/appErr.js');
const getTokenFromHeader = require('../utils/getTokenFromHeader.js');
const verifyToken = require('../utils/verifyToken.js');

// eslint-disable-next-line consistent-return
const isLogin = (req, res, next) => {
  // get token from req header
  const token = getTokenFromHeader(req);
  // verify the extracted token
  const decodedUser = verifyToken(token);
  // save the user into req obj, extracting the id from decoded token and setting it in the req.user property
  req.user = decodedUser.id;
  if (!decodedUser) {
    return next(new AppErr('Invalid/Expired Token, Please login again', 401));
  }
  next();
};

module.exports = isLogin;
