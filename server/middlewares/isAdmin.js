const User = require('../model/User.js');
const { AppErr } = require('../utils/appErr.js');

// and make sure to use after isLogin middlwr
const isAdmin = async (req, res, next) => {
  const user = await User.findById(req.user);

  if (user && user.isAdmin === true) {
    // if admin then go for it
    return next();
  }
  // no admin
  return next(new AppErr(`${req.user} is not an admin`, 401));
};

module.exports = isAdmin;
