/* eslint-disable consistent-return */
const bcrypt = require('bcryptjs');
const User = require('../../model/User.js');
const { AppErr, appErr } = require('../../utils/appErr.js');
const generateToken = require('../../utils/generateToken.js');
const Style = require('../../model/Style.js');

// Register
const registerUserCtrl = async (req, res, next) => {
  const { fullname, password, email, isAdmin, profilePicture } = req.body;
  try {
    // check if email exist
    const userFound = await User.findOne({ email });
    if (userFound) {
      return next(appErr('User already exists'));
    }

    // hash password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    // create user in the database
    const user = await User.create({
      fullname,
      email,
      password: hashedPassword,
      isAdmin,
      profilePicture,
    });
    res.json({
      // if user is created in databse res to the http req with json object containig the details
      status: 'success',
      fullname: user.fullname,
      email: user.email,
      id: user._id,
      isAdmin: user.isAdmin,
      profilePicture: user.profilePicture,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// login
const userLoginCtrl = async (req, res, next) => {
  const { email, password } = req.body;
  try {
    // check if email exists
    const userFound = await User.findOne({ email });
    if (!userFound) return next(new AppErr('Invalid login credentials', 400));

    // check for password validity
    const isPasswordMatch = await bcrypt.compare(password, userFound.password);
    if (!isPasswordMatch)
      return next(new AppErr('Invalid login credentials', 400));

    res.json({
      status: 'success',
      fullname: userFound.fullname,
      id: userFound._id,
      token: generateToken(userFound._id),
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// profile single and his styles and nested genres
const userProfileCtrl = async (req, res, next) => {
  // console.log(req.user);
  // console.log("err");
  try {
    const user = await User.findById(req.user).populate({
      path: 'styles',
      populate: {
        path: 'genres',
        model: 'Genre',
      },
    });
    res.json({
      // change
      status: 'success',
      data: user,
    });
  } catch (error) {
    console.log(new AppErr(error.message, 500));
    next(new AppErr(error.message, 500));
  }
};

// all profiles
const getProfilesCtrl = async (req, res, next) => {
  try {
    const profiles = await User.find();
    res.status(200).json({
      status: 'success',
      data: profiles,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// get user name based on style id
const getUserNameByUserId = async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = await User.findById(id);
    res.status(200).json({
      name: user.fullname,
      profilePicture: user.profilePicture,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// delete
const deleteUserCtrl = async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = await User.findById(id);

    if (!user) {
      return res.status(404).json({
        status: 'failed',
        message: 'User not found',
      });
    }

    // Check if the user has a styleId before trying to delete styles
    if (user.styleId) {
      // Delete user styles
      await Style.deleteMany({ createdBy: user._id });
    }

    // Delete the user
    await User.findByIdAndDelete(id);

    res.status(200).json({
      status: 'success',
      data: id,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// update
const updateUserCtrl = async (req, res, next) => {
  try {
    const newUserData = await User.findById(req.user);

    // check if email(the new email) exist
    if (req.body.email) {
      const userFound = await User.findOne({ email: req.body.email });
      if (userFound) {
        return next(
          new AppErr('Email is taken or you already have this email', 400)
        );
      }

      newUserData.email = req.body.email;
    }

    if (req.body.fullname) {
      newUserData.fullname = req.body.fullname;
    }

    // check if user is updating the password
    if (req.body.password) {
      const salt = await bcrypt.genSalt(10);
      const hashedPassword = await bcrypt.hash(req.body.password, salt); // hashing the new password

      newUserData.password = hashedPassword;
    }
    // if user is not updating password, but is making changes to other user details
    const user = await User.findByIdAndUpdate(
      req.user,
      {
        email: newUserData.email,
        password: newUserData.password,
        fullname: newUserData.fullname,
      },
      {
        new: true,
        runValidators: true,
      }
    );
    // send the response
    res.status(200).json({
      status: 'success',
      data: user,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

module.exports = {
  registerUserCtrl,
  userLoginCtrl,
  userProfileCtrl,
  deleteUserCtrl,
  updateUserCtrl,
  getProfilesCtrl,
  getUserNameByUserId,
};
