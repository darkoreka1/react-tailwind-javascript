const Style = require('../../model/Style.js');
const Genre = require('../../model/Genre.js');
const User = require('../../model/User.js');
const { AppErr } = require('../../utils/appErr.js');

// create
const createGenreCtrl = async (req, res, next) => {
  const { name, genreType, style, notes } = req.body;
  try {
    // 1.Find user
    const userFound = await User.findById(req.user);
    if (!userFound) return next(new AppErr('User not found', 404));
    // 2.Find the style
    const styleFound = await Style.findById(style);
    if (!styleFound) return next(new AppErr('Style not found', 404));

    // 3.Create the genre
    const genre = await Genre.create({
      name,
      genreType,
      style,
      notes,
      createdBy: req.user,
    });

    // 4.Push the genre to the style to make the relationship between them
    styleFound.genres.push(genre._id);

    // 5.save the updated syle with the new genre reference to the database
    await styleFound.save();

    res.json({ status: 'success', data: genre });
  } catch (error) {
    // nem itt a hiba
    res.json(error);
  }
};

// all
const getGenresCtrl = async (req, res, next) => {
  try {
    const genr = await Genre.find();
    res.status(200).json({
      status: 'success',
      data: genr,
    });
  } catch (error) {
    // nem itt a hiba
    next(new AppErr(error.message, 500));
  }
};

// single
const getGenreCtrl = async (req, res, next) => {
  try {
    const { id } = req.params;
    const genr = await Genre.findById(id);
    res.json({ status: 'success', data: genr });
  } catch (error) {
    console.log(error);
    next(new AppErr(error.message, 500));
  }
};

// delete
const deleteGenreCtrl = async (req, res, next) => {
  try {
    const { id } = req.params;
    await Genre.findByIdAndDelete(id);
    res.json({
      status: 'success',
      data: null,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// update
const updateGenreCtrl = async (req, res, next) => {
  try {
    const { id } = req.params;
    const genr = await Genre.findByIdAndUpdate(id, req.body, {
      new: true,
      runValidators: true,
    });
    res.json({
      status: 'success',
      data: genr,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

module.exports = {
  createGenreCtrl,
  getGenresCtrl,
  getGenreCtrl,
  deleteGenreCtrl,
  updateGenreCtrl,
};
