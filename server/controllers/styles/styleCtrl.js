/* eslint-disable consistent-return */
// acc helyett

const Style = require('../../model/Style.js');
const User = require('../../model/User.js');
const { AppErr } = require('../../utils/appErr.js');

// create
const createStyleCtrl = async (req, res, next) => {
  const { name, styleType, category, notes, imageUrls } = req.body; // get data from body
  try {
    // 1.find the logged in user
    const userFound = await User.findById(req.user);
    if (!userFound) return next(new AppErr('User not found', 404));

    // 2.create the style
    const style = await Style.create({
      name,
      styleType,
      category,
      notes,
      createdBy: req.user,
      imageUrls,
    });

    // 3.push the style into users style field
    userFound.styles.push(style._id);

    // 4.resave the user
    await userFound.save();
    res.json({
      status: 'success',
      data: style,
    });
  } catch (error) {
    next(error);
  }
};

// get all styles from database
const getStylesCtrl = async (req, res) => {
  try {
    // the populate genres replaces the reference(id) in the document with the actual data from another collection
    const styles = await Style.find().populate('genres'); // get all records in the Style collection and return in array
    res.json(styles); // style array sending
  } catch (error) {
    res.json(error);
  }
};

// single
const getStyleCtrl = async (req, res, next) => {
  try {
    // find the id from params
    const { id } = req.params;
    // console.log("Requested Style ID:", id);
    const style = await Style.findById(id).populate('genres');
    // console.log("Retrieved Style:", style);
    res.json({
      status: 'success',
      data: style,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// delete
const deleteStyleCtrl = async (req, res, next) => {
  try {
    const { id } = req.params;
    await Style.findByIdAndDelete(id);
    res.status(200).json({
      status: 'success',
      data: null,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// update
const updateStyleCtrl = async (req, res, next) => {
  try {
    const { id } = req.params;
    const style = await Style.findByIdAndUpdate(id, req.body, {
      new: true,
      runValidators: true,
    });
    res.json({
      status: 'success',
      data: style,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// Like a style
const likeStyleCtrl = async (req, res, next) => {
  const { styleId } = req.body; // Get the style ID from the request body
  try {
    const style = await Style.findById(styleId);

    // Check if the user has already liked this style
    if (style.likes.includes(req.user)) {
      return next(new AppErr('User already liked this style', 400));
    }

    // Add the user's ID to the likes array
    style.likes.push(req.user);

    await style.save();

    res.json({
      status: 'success',
      data: style,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

// Unlike a style
const unlikeStyleCtrl = async (req, res, next) => {
  const { styleId } = req.body; // Get the style ID from the request body
  try {
    const style = await Style.findById(styleId);

    // Check if the user has liked this style
    const userIndex = style.likes.indexOf(req.user);
    if (userIndex === -1) {
      return next(new AppErr('User has not liked this style', 400));
    }

    // Remove the user's ID from the likes array
    style.likes.splice(userIndex, 1);

    await style.save();

    res.json({
      status: 'success',
      data: style,
    });
  } catch (error) {
    next(new AppErr(error.message, 500));
  }
};

module.exports = {
  createStyleCtrl,
  getStyleCtrl,
  deleteStyleCtrl,
  updateStyleCtrl,
  getStylesCtrl,
  likeStyleCtrl,
  unlikeStyleCtrl,
};
